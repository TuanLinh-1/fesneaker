import * as types from '../constant/SizeConstant'

function getSizeRequest(payload){
    return{
        type:types.GET_SIZE_REQUEST,
        payload
    }
}

function getSizeSuccess(payload){
    return{
        type:types.GET_SIZE_SUCCESS,
        payload
    }
}

function getSizeFailure(payload){
    return{
        type:types.GET_SIZE_FAILURE,
        payload
    }
}
//////////////////////////////////
function addSizeRequest(payload){
    return{
        type:types.ADD_SIZE_REQUEST,
        payload
    }
}

function addSizeSuccess(payload){
    return{
        type:types.ADD_SIZE_SUCCESS,
        payload
    }
}

function addSizeFailure(payload){
    return{
        type:types.ADD_SIZE_FAILURE,
        payload
    }
}
////////////////////////////////////////
function updateSizeRequest(payload){
    return{
        type:types.UPDATE_SIZE_REQUEST,
        payload
    }
}

function updateSizeSuccess(payload){
    return{
        type:types.UPDATE_SIZE_SUCCESS,
        payload
    }
}

function updateSizeFailure(payload){
    return{
        type:types.UPDATE_SIZE_FAILURE,
        payload
    }
}
//////////////////////////////////////////
function deleteSizeRequest(payload){
    return{
        type:types.DELETE_SIZE_REQUEST,
        payload
    }
}

function deleteSizeSuccess(payload){
    return{
        type:types.DELETE_SIZE_SUCCESS,
        payload
    }
}

function deleteSizeFailure(payload){
    return{
        type:types.DELETE_SIZE_FAILURE,
        payload
    }
}
export{
    getSizeRequest,
    getSizeSuccess,
    getSizeFailure,

    addSizeRequest,
    addSizeSuccess,
    addSizeFailure,

    updateSizeRequest,
    updateSizeSuccess,
    updateSizeFailure,

    deleteSizeRequest,
    deleteSizeSuccess,
    deleteSizeFailure
}