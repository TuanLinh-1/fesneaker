import * as types from "../constant/SizeConstant";
const DEFAULT_STATE = {
  listData: [],
  isFetching: false,
  dataFetched: false,
  error: false,
  errorMessage: "",
};
export default (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case types.GET_SIZE_REQUEST:
    case types.ADD_SIZE_REQUEST:
    case types.UPDATE_SIZE_REQUEST:
    case types.DELETE_SIZE_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_SIZE_SUCCESS:
    case types.ADD_SIZE_SUCCESS:
    case types.UPDATE_SIZE_SUCCESS:
    case types.DELETE_SIZE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataFetched: true,
        listData: action.payload,
      };
    case types.GET_SIZE_FAILURE:
    case types.ADD_SIZE_FAILURE:
    case types.UPDATE_SIZE_FAILURE:
    case types.DELETE_SIZE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        errorMessage: action.payload,
      };
    default:
      return state;
  }
};
