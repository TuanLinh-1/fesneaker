import React, { useState } from "react";
import SizeContainer from "../container/SizeContainer";
import ColorContainer from "../container/ColorContainer";
import SanPhamContainer from'../container/SanPhamContrainer'
import classes from "../css/HomePage.module.css";

import {
  //   DesktopOutlined,
  //   FileOutlined,
  //   PieChartOutlined,
  //   TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, theme } from "antd";
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  //   getItem('Option 1', '1', <PieChartOutlined />),
  //   getItem('Option 2', '2', <DesktopOutlined />),
  getItem("Quản lí sản phẩm", "QLSP", <UserOutlined />, [
    getItem("Size", "size"),
    getItem("Color", "color"),
    getItem("Sản phẩm", "sanpham"),
  ]),
  //   getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
  //   getItem('Files', '9', <FileOutlined />),
];
const HomePage = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const [collapsed, setCollapsed] = useState(false);
  const [activeComponent, setActiveComponent] = useState("1");

  const renderComponent = () => {
    switch (activeComponent) {
      case "size":
        return <SizeContainer />;
      case "color":
        return <ColorContainer />;
        case "sanpham":
          return <SanPhamContainer />;
      default:
        return null;
    }
  };

  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className={classes["demo-logo-vertical"]} />
        <Menu
          theme="dark"
          defaultSelectedKeys={["1"]}
          mode="inline"
          items={items}
          onClick={(value)=>{setActiveComponent(value.key)}} //value gồm  key , keyPath, selectedKeys, domEvent 
        />
      </Sider>
      <Layout>
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          Quản lí WebSite Bán Giày Sneaker F4
        </Header>
        <Content
          style={{
            margin: "0 16px",
          }}
        >
          <Breadcrumb
            style={{
              margin: "16px 0",
            }}
          >
            <Breadcrumb.Item>User</Breadcrumb.Item>
            <Breadcrumb.Item>Bill</Breadcrumb.Item>
          </Breadcrumb>
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
            {renderComponent()}
          </div>
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}
        >
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};
export default HomePage;
