import React,{Component} from 'react';
import './App.css';
import RouterRoots from './Router';

class App extends Component{
  render(){
    return(
      <div className='App'>
        <RouterRoots/>
      </div>
    )
  }
}

export default App;
