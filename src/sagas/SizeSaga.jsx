import * as types from "../constant/SizeConstant";
import * as actions from "../actions/SizeAction";
import * as SizeApi from "../fetchApi/SizeApi";
import { put, takeEvery } from "redux-saga/effects";

function* getSize() {
  try {
    let res = yield SizeApi.getData();
    yield put(actions.getSizeSuccess(res.content));
  } catch (error) {
    yield put(actions.getSizeFailure());
  }
}

function* addSize(action) {
  console.log("Đã vào saga",action.payload);
  try {
    yield SizeApi.addData(action.payload);
    yield put(actions.addSizeSuccess());
    yield put(actions.getSizeRequest());
  } catch (error) {
    yield put(actions.addSizeFailure(error.message));
  }
}
function* updateSize(action) {
  try {
    yield SizeApi.updateData(action.payload);
    yield put(actions.updateSizeSuccess());
    yield put(actions.getSizeRequest());
  } catch (error) {
    yield put(actions.updateSizeFailure(error.message));
  }
}
function* deleteSize(action) {
  try {
    yield SizeApi.addData(action.payload);
    yield put(actions.deleteSizeSuccess());
    yield put(actions.getSizeRequest());
  } catch (error) {
    yield put(actions.deleteSizeFailure(error.message));
  }
}

export const SizeSaga = [
  takeEvery(types.GET_SIZE_REQUEST, getSize),
  takeEvery(types.ADD_SIZE_REQUEST, addSize),
  takeEvery(types.UPDATE_SIZE_REQUEST, updateSize),
  takeEvery(types.DELETE_SIZE_REQUEST, deleteSize),
];
