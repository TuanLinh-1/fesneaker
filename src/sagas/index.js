import {all} from "redux-saga/effects"
import  {SizeSaga} from'../sagas/SizeSaga'
function* rootSaga(){
    yield all([
        ...SizeSaga
    ]);
}
export default rootSaga