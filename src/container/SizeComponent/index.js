import React from "react";

import { Layout, Select, Space, Button, Input, Table, Pagination } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import ModalAddOrUpdateSize from "./SizeModal/ModalAddOrUpdateSize";
const { Content } = Layout;
const { Search } = Input;
const onSearch = (value, _e, info) => console.log(info?.source, value);
class Size extends React.Component {
  state = {
    isOpenModalAddOrUpdateSize: false,
    size: {
      id: "",
      size: "",
      moTa: "",
      trangThai: false,
    },
    action: {
      type: "",
      title: "",
    },
  };
  AddAction = {
    type: "ADD",
    title: "Thêm mới Size",
  };
  UpdateAction = {
    type: "UPDATE",
    title: "Cập nhật Size",
  };
  ///////////////////////////////
  setOpenModalAddOrUpdateSize = (record, newAction) => {
    this.setState({
      ...this.state,
      isOpenModalAddOrUpdateSize: true,
      size: {
        ...this.state.size,
        id: record.id,
        size: record.size,
        moTa: record.moTa,
        trangThai: record.trangThai,
      },
      action: newAction,
    });
  };
  ///////////////////////////////

  setCloseModalAddOrUpdateSize = () => {
    this.setState({
      ...this.state,
      isOpenModalAddOrUpdateSize: false,
    });
  };
  ///////////////////////////////

  handleChangeSize = (e) => {
    this.setState(
      {
        ...this.state,
        size: {
          ...this.state.size,
          size: e.target.value,
        },
      },
      () => {
        console.log(this.state.size.size);
      }
    );
  };
  ///////////////////////////////
  handleChangeMoTa = (e) => {
    this.setState(
      {
        ...this.state,
        size: {
          ...this.state.size,
          moTa: e.target.value,
        },
      },
      () => {
        console.log(this.state.size.moTa);
      }
    );
  };
  ////////////////////////////////
  handleChangeTrangThai = (value) => {
    this.setState(
      {
        ...this.state,
        size: {
          ...this.state.size,
          trangThai: value,
        },
      },
      () => {
        console.log(this.state.size.trangThai);
      }
    );
  };

  render() {
    const { data,addSize,updateSize,deleteSize } = this.props;
    let listSize = [];
    if (data) {
      listSize = data.map((item, index) => {
        return {
          key: index,
          stt: index + 1,
          id: item.id,
          size: item.size,
          moTa: item.moTa,
          trangThai: item.trangThai,
        };
      });
    }
    console.log(data,"aaaaaa");
    console.log("propppp",addSize);
    // Tạo column
    const columns = [
      {
        title: "STT",
        dataIndex: "stt",
        key: "stt",
      },
      {
        title: "Size",
        dataIndex: "size",
        key: "size",
      },
      {
        title: "Mô tả",
        dataIndex: "moTa",
        key: "moTa",
      },
      {
        title: "Trạng Thái",
        dataIndex: "trangThai",
        key: "trangThai",
      },
      {
        title: "Hành Động",
        key: "action",
        render: (_, record) => {
          if (!record || !record.id) {
            return null;
          }
          return (
            <Space size="middle">
              <a
                style={{ color: "orange" }}
                onClick={(e) => {
                  e.stopPropagation();
                  this.setOpenModalAddOrUpdateSize(record,this.UpdateAction);
                }}
              >
                <EditOutlined /> Sửa
              </a>
              <a
                style={{ color: "red" }}
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                <DeleteOutlined />
                Xóa
              </a>
            </Space>
          );
        },
      },
    ];

    return (
      <Content style={{ padding: "0 50px" }}>
        <div>
          <h1 style={{ textAlign: "left", fontSize: "40px " }}>Quản lý Size</h1>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            height: "fit-content",
          }}
        >
          <Button
            type="primary"
            onClick={(record) => {
              
              this.setOpenModalAddOrUpdateSize(record,this.AddAction);
            }}
          >
            Thêm Mới
          </Button>

          <ModalAddOrUpdateSize
            isOpenModalAddOrUpdateSize={this.state.isOpenModalAddOrUpdateSize}
            setCloseModalAddOrUpdateSize={this.setCloseModalAddOrUpdateSize}
            size={this.state.size}
            handleChangeSize={this.handleChangeSize}
            handleChangeMoTa={this.handleChangeMoTa}
            handleChangeTrangThai={this.handleChangeTrangThai}
            action={this.state.action}
            addSize={addSize}
            updateSize={updateSize}
          />

          <Space wrap style={{}}>
            <Select
              defaultValue="Sắp xếp"
              style={{ width: "150px", height: "100%" }}
              // onChange={handleChangeS}
              options={[
                {
                  value: "1",
                  label: "Từ a đến z",
                },
                {
                  value: "2",
                  label: "Từ z đến a",
                },
              ]}
            />
            <Search
              placeholder="input search text"
              onSearch={onSearch}
              style={{ width: "300px" }}
            />
          </Space>
        </div>
        <Table
          style={{ marginTop: "50px" }}
          bordered
          columns={columns}
          dataSource={listSize}
          pagination={false}
          // onRow={(record) => {
          //   return {
          //     onClick: () => this.handleOpenModalTask(record),
          //   };
          // }}
        />

        <Pagination
          style={{ display: "flex", justifyContent: "end", margin: "50px 0" }}
          defaultCurrent={1}
          total={50}
        />
      </Content>
    );
  }
}
export default Size;
