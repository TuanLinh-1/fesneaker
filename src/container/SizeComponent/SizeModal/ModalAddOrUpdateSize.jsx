import { Select, Space, Modal } from "antd";

const ModalAddOrUpdateSize = (props) => {
  const {
    isOpenModalAddOrUpdateSize,
    setCloseModalAddOrUpdateSize,
    size,
    handleChangeSize,
    handleChangeMoTa,
    handleChangeTrangThai,
    action,
    addSize,
    updateSize,
  } = props;
  return (
    <Modal
      style={{ position: "relative", top: "300px" }}
      title={
        <div
          style={{
            fontSize: "20px",
            color: "green",
          }}
        >
          {action.title}
        </div>
      }
      open={isOpenModalAddOrUpdateSize}
      onOk={() => {
        if (action.type === "ADD") {
          console.log("ADD", addSize);
          addSize(size);
        }
        if (action.type === "UPDATE") {
          console.log("UPDATE");
          updateSize(size);
        }
        setCloseModalAddOrUpdateSize()
      }}
      onCancel={setCloseModalAddOrUpdateSize}
    >
      <div
        style={{
          marginTop: "20px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <label style={{ fontSize: "18px" }}>Size:</label>
        <input
          style={{ marginLeft: "10px", width: "333px" }}
          type="text"
          value={size.size || ""}
          onChange={handleChangeSize}
        />
      </div>
      <div
        style={{
          marginTop: "20px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <label style={{ fontSize: "18px" }}>Mô tả:</label>
        <input
          style={{ marginLeft: "10px", width: "333px" }}
          type="text"
          value={size.moTa || ""}
          onChange={handleChangeMoTa}
        />
      </div>

      <div
        style={{
          margin: "30px 0",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <label style={{ fontSize: "18px" }}>Trạng thái:</label>
        <Space wrap>
          <Select
            defaultValue="Chọn"
            style={{ marginLeft: "10px", width: "340px" }}
            value={size.trangThai || "Chọn"}
            onChange={handleChangeTrangThai}
            options={[
              {
                value: true,
                label: "Hoạt động",
              },
              {
                value: false,
                label: "Hết hạn",
              },
            ]}
          />
        </Space>
      </div>
    </Modal>
  );
};
export default ModalAddOrUpdateSize;
