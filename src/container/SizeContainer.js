import React from "react";
import Size from "./SizeComponent";
import * as actions from "../actions/SizeAction";
import { connect } from "react-redux";
class SizeContainer extends React.Component {
  componentDidMount() {
    this.props.initLoad();
  }
  render() {
    return <Size {...this.props} />;
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.SizeReducer.listData,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    initLoad: () => {
      dispatch(actions.getSizeRequest());
    },
    addSize: (data) => {
      dispatch(actions.addSizeRequest(data));
    },
    updateSize: (data) => {
      dispatch(actions.updateSizeRequest(data));
    },
    deleteSize: (data) => {
      dispatch(actions.deleteSizeRequest(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SizeContainer);
