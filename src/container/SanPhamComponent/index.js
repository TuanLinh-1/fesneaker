import React from "react";
import { Layout, Select, Space, Button, Input, Table, Pagination } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
const { Content } = Layout;
const { Search } = Input;
const onSearch = (value, _e, info) => console.log(info?.source, value);
const columns = [
  {
    title: "STT",
    dataIndex: "stt",
    key: "stt",
  },
  {
    title: "Mã Sản Phẩm",
    dataIndex: "MSP",
    key: "MSP",
  },
  {
    title: "Tên Sản Phẩm ",
    dataIndex: "TSP",
    key: "TSP",
  },
  {
    title: "Mô tả",
    dataIndex: "moTa",
    key: "moTa",
  },
  {
    title: "Trạng Thái",
    dataIndex: "trangThai",
    key: "trangThai",
  },
  {
    title: "Hành Động",
    key: "action",
    render: (_, record) => {
      if (!record || !record.id) {
        return null;
      }
      return (
        <Space size="middle">
          <a
            style={{ color: "orange" }}
            onClick={(e) => {
              e.stopPropagation();
              this.setOpenModalAddOrUpdateSize(record, this.UpdateAction);
            }}
          >
            <EditOutlined /> Sửa
          </a>
          <a
            style={{ color: "red" }}
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <DeleteOutlined />
            Xóa
          </a>
        </Space>
      );
    },
  },
];
const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"],
  },
];
const SanPham = () => {
  return (
    <Content style={{ padding: "0 50px" }}>
      <div>
        <h1 style={{ textAlign: "left", fontSize: "40px " }}>Sản phảm</h1>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          height: "fit-content",
        }}
      >
        <Button type="primary">Thêm Mới</Button>

        <Space wrap style={{}}>
          <Select
            defaultValue="Sắp xếp"
            style={{ width: "150px", height: "100%" }}
            // onChange={handleChangeS}
            options={[
              {
                value: "1",
                label: "Từ a đến z",
              },
              {
                value: "2",
                label: "Từ z đến a",
              },
            ]}
          />
          <Search
            placeholder="input search text"
            onSearch={onSearch}
            style={{ width: "300px" }}
          />
        </Space>
      </div>
      <Table
        style={{ marginTop: "50px" }}
        bordered
        columns={columns}
        //   dataSource={listSize}
        //   pagination={false}
        // onRow={(record) => {
        //   return {
        //     onClick: () => this.handleOpenModalTask(record),
        //   };
        // }}
      />

      <Pagination
        style={{ display: "flex", justifyContent: "end", margin: "50px 0" }}
        defaultCurrent={1}
        total={50}
      />
    </Content>
  );
};
export default SanPham;
